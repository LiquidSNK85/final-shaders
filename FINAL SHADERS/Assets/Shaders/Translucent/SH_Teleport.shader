// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "SH_Teleport"
{
	Properties
	{
		_Cutoff( "Mask Clip Value", Float ) = 0.5
		_Height("Height", Range( 0.6 , 1.25)) = 0.6
		_Diffuse("Diffuse", 2D) = "white" {}
		_EMSAO("EMSAO", 2D) = "white" {}
		_Normal("Normal", 2D) = "white" {}
		_Color0("Color 0", Color) = (0,0,0,0)
		[HideInInspector] _texcoord( "", 2D ) = "white" {}
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "TransparentCutout"  "Queue" = "Geometry+0" "IsEmissive" = "true"  }
		Cull Off
		CGPROGRAM
		#include "UnityShaderVariables.cginc"
		#pragma target 3.0
		#pragma surface surf Standard keepalpha addshadow fullforwardshadows 
		struct Input
		{
			float2 uv_texcoord;
			half ASEVFace : VFACE;
			float3 worldPos;
		};

		uniform sampler2D _Normal;
		uniform float4 _Normal_ST;
		uniform sampler2D _Diffuse;
		uniform float4 _Diffuse_ST;
		uniform float4 _Color0;
		uniform sampler2D _EMSAO;
		uniform float4 _EMSAO_ST;
		uniform float _Height;
		uniform float _Cutoff = 0.5;

		void surf( Input i , inout SurfaceOutputStandard o )
		{
			float2 uv_Normal = i.uv_texcoord * _Normal_ST.xy + _Normal_ST.zw;
			o.Normal = tex2D( _Normal, uv_Normal ).rgb;
			float2 uv_Diffuse = i.uv_texcoord * _Diffuse_ST.xy + _Diffuse_ST.zw;
			float4 ifLocalVar35 = 0;
			if( i.ASEVFace > 0.0 )
				ifLocalVar35 = tex2D( _Diffuse, uv_Diffuse );
			else if( i.ASEVFace < 0.0 )
				ifLocalVar35 = _Color0;
			o.Albedo = ifLocalVar35.rgb;
			float2 uv_EMSAO = i.uv_texcoord * _EMSAO_ST.xy + _EMSAO_ST.zw;
			float4 tex2DNode26 = tex2D( _EMSAO, uv_EMSAO );
			float3 temp_cast_2 = (tex2DNode26.r).xxx;
			o.Emission = temp_cast_2;
			o.Metallic = tex2DNode26.g;
			o.Smoothness = tex2DNode26.b;
			o.Occlusion = tex2DNode26.a;
			o.Alpha = 1;
			float3 ase_vertex3Pos = mul( unity_WorldToObject, float4( i.worldPos , 1 ) );
			float ifLocalVar29 = 0;
			if( ase_vertex3Pos.y > _Height )
				ifLocalVar29 = ( 1.23 + _Height );
			else if( ase_vertex3Pos.y < _Height )
				ifLocalVar29 = ( 1.23 - _Height );
			float ifLocalVar19 = 0;
			if( ifLocalVar29 > ase_vertex3Pos.y )
				ifLocalVar19 = 1.0;
			else if( ifLocalVar29 < ase_vertex3Pos.y )
				ifLocalVar19 = 0.0;
			clip( ifLocalVar19 - _Cutoff );
		}

		ENDCG
	}
	Fallback "Diffuse"
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=15401
0;73;1048;432;900.0721;529.6617;1.354386;True;True
Node;AmplifyShaderEditor.RangedFloatNode;17;-964.6945,363.2383;Float;False;Property;_Height;Height;1;0;Create;True;0;0;False;0;0.6;0;0.6;1.25;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;31;-921.7801,234.4847;Float;False;Constant;_MidPoint;MidPoint;5;0;Create;True;0;0;False;0;1.23;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.PosVertexDataNode;25;-744.1609,112.339;Float;False;0;0;5;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleAddOpNode;32;-577.931,354.9177;Float;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleSubtractOpNode;33;-587.4304,500.5845;Float;False;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;20;-1007.275,-495.5354;Float;True;Property;_Diffuse;Diffuse;2;0;Create;True;0;0;False;0;None;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;21;-390.3829,445.3447;Float;False;Constant;_Float0;Float 0;4;0;Create;True;0;0;False;0;1;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.ColorNode;53;-543.8687,-241.1772;Float;False;Property;_Color0;Color 0;5;0;Create;True;0;0;False;0;0,0,0,0;0,0,0,0;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.FaceVariableNode;51;-395.862,-465.0343;Float;False;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;24;-417.0621,525.4384;Float;False;Constant;_Float1;Float 1;4;0;Create;True;0;0;False;0;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.ConditionalIfNode;29;-403.7206,185.7826;Float;False;False;5;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;4;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;52;-433.1406,-345.2009;Float;False;Constant;_Float2;Float 2;6;0;Create;True;0;0;False;0;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.ConditionalIfNode;19;-141.6126,307.9451;Float;False;False;5;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;4;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;26;-1011.589,-79.8219;Float;True;Property;_EMSAO;EMSAO;3;0;Create;True;0;0;False;0;None;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ConditionalIfNode;35;-144.889,-418.4121;Float;False;False;5;0;FLOAT;0;False;1;FLOAT;0;False;2;COLOR;0,0,0,0;False;3;COLOR;0,0,0,0;False;4;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SamplerNode;27;-1007.275,-288.6738;Float;True;Property;_Normal;Normal;4;0;Create;True;0;0;False;0;None;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;0;348.0121,-85.52305;Float;False;True;2;Float;ASEMaterialInspector;0;0;Standard;SH_Teleport;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;Off;0;False;-1;0;False;-1;False;0;False;-1;0;False;-1;False;0;Custom;0.5;True;True;0;True;TransparentCutout;;Geometry;All;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;0;False;-1;False;0;False;-1;255;False;-1;255;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;False;2;15;10;25;False;0.5;True;0;0;False;-1;0;False;-1;0;0;False;-1;0;False;-1;-1;False;-1;-1;False;-1;0;False;0;0,0,0,0;VertexOffset;True;False;Cylindrical;False;Relative;0;;0;-1;-1;-1;0;False;0;0;False;-1;-1;0;False;-1;0;0;16;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT;0;False;4;FLOAT;0;False;5;FLOAT;0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0;False;9;FLOAT;0;False;10;FLOAT;0;False;13;FLOAT3;0,0,0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;32;0;31;0
WireConnection;32;1;17;0
WireConnection;33;0;31;0
WireConnection;33;1;17;0
WireConnection;29;0;25;2
WireConnection;29;1;17;0
WireConnection;29;2;32;0
WireConnection;29;4;33;0
WireConnection;19;0;29;0
WireConnection;19;1;25;2
WireConnection;19;2;21;0
WireConnection;19;4;24;0
WireConnection;35;0;51;0
WireConnection;35;1;52;0
WireConnection;35;2;20;0
WireConnection;35;4;53;0
WireConnection;0;0;35;0
WireConnection;0;1;27;0
WireConnection;0;2;26;1
WireConnection;0;3;26;2
WireConnection;0;4;26;3
WireConnection;0;5;26;4
WireConnection;0;10;19;0
ASEEND*/
//CHKSM=C5DA93B6604CF7D655AEB11717BB5D9A754046BA